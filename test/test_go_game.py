import unittest
from src.game_logic.board import Board
from src.game_logic.cell_statuses import CellStatuses


class TestGoGame(unittest.TestCase):
    """
    WBEE
    BEEE
    EEEE
    EEEE
    Белая фигура должна быть мертва
    """
    def test_if_dead_in_angle(self):
        desk = Board(4)
        desk._board[0][0].set_status(CellStatuses.WHITE)
        desk._board[0][1].set_status(CellStatuses.BLACK)
        desk._board[1][0].set_status(CellStatuses.BLACK)

        self.assertEqual(desk.if_dead({(0, 0)}, CellStatuses.WHITE, (0, 0)), {(0, 0)})

    '''
    EBEE
    BWBE
    EBEE
    EEEE
    Белая фигура должна быть мертва
    '''
    def test_if_dead_between_four_figures(self):
        desk = Board(4)
        desk._board[1][1].set_status(CellStatuses.WHITE)
        desk._board[0][1].set_status(CellStatuses.BLACK)
        desk._board[1][0].set_status(CellStatuses.BLACK)
        desk._board[1][2].set_status(CellStatuses.BLACK)
        desk._board[2][1].set_status(CellStatuses.BLACK)

        self.assertEqual(desk.if_dead({(1, 1)}, CellStatuses.WHITE, (1, 1)), {(1, 1)})

    '''
    EWBE
    WBEE
    BBEE
    EEEE
    Обе белые фигуры должны быть живы
    '''
    def test_if_dead_with_eye(self):
        desk = Board(4)
        desk._board[0][1].set_status(CellStatuses.WHITE)
        desk._board[1][0].set_status(CellStatuses.WHITE)
        desk._board[0][2].set_status(CellStatuses.BLACK)
        desk._board[1][1].set_status(CellStatuses.BLACK)
        desk._board[2][0].set_status(CellStatuses.BLACK)

        self.assertFalse(desk.if_dead({(0, 1)}, CellStatuses.WHITE, (0, 1)))
        self.assertFalse(desk.if_dead({(1, 0)}, CellStatuses.WHITE, (1, 0)))
    '''
    WWBE
    WBEE
    BBEE
    EEEE
    Все белые фигуры должны быть мертвы
    '''
    def test_if_dead_group(self):
        desk = Board(4)
        desk._board[0][1].set_status(CellStatuses.WHITE)
        desk._board[1][0].set_status(CellStatuses.WHITE)
        desk._board[0][0].set_status(CellStatuses.WHITE)
        desk._board[0][2].set_status(CellStatuses.BLACK)
        desk._board[1][1].set_status(CellStatuses.BLACK)
        desk._board[2][0].set_status(CellStatuses.BLACK)

        result = desk.if_dead({(0, 1)}, CellStatuses.WHITE, (0, 1))
        self.assertTrue(result.__contains__((0, 0)))
        self.assertTrue(result.__contains__((0, 1)))
        self.assertTrue(result.__contains__((1, 0)))

    '''
    WWBE
    WBEE
    BBEE
    EEEE
    Все клетки, где стоят белые фигуры должны получить статус EMPTY.
    '''
    def test_update_for_group_in_angle(self):
        desk = Board(4)

        desk._board[0][1].set_status(CellStatuses.WHITE)
        desk._board[1][0].set_status(CellStatuses.WHITE)
        desk._board[0][0].set_status(CellStatuses.WHITE)
        desk._board[0][2].set_status(CellStatuses.BLACK)
        desk._board[1][1].set_status(CellStatuses.BLACK)
        desk._board[2][0].set_status(CellStatuses.BLACK)

        desk.update(CellStatuses.BLACK)

        self.assertTrue(desk._board[1][0].check_status(CellStatuses.EMPTY))
        self.assertTrue(desk._board[0][0].check_status(CellStatuses.EMPTY))
        self.assertTrue(desk._board[0][1].check_status(CellStatuses.EMPTY))

    '''
    EWEE
    WBWE
    BEBE
    EBEE
    
    EWEE
    WBWE
    BWBE
    EBEE
    Белые сходили в [2][0], черная фишка в [1][1] должна быть мертва
    '''
    def test_update_for_controversial_situation_figure(self):
        desk = Board(4)
        desk._board[0][1].set_status(CellStatuses.WHITE)
        desk._board[1][0].set_status(CellStatuses.WHITE)
        desk._board[1][2].set_status(CellStatuses.WHITE)
        desk._board[2][1].set_status(CellStatuses.WHITE)
        desk._board[1][1].set_status(CellStatuses.BLACK)
        desk._board[2][0].set_status(CellStatuses.BLACK)
        desk._board[2][2].set_status(CellStatuses.BLACK)
        desk._board[3][1].set_status(CellStatuses.BLACK)
        desk.update(CellStatuses.WHITE)
        self.assertTrue(desk._board[1][1].check_status(CellStatuses.EMPTY))
        self.assertFalse(desk._board[2][0].check_status(CellStatuses.EMPTY))

    def test_update_for_group(self):
        desk = Board(13)
        desk._board[9][10].set_status(CellStatuses.WHITE)
        desk._board[8][10].set_status(CellStatuses.BLACK)
        desk._board[10][10].set_status(CellStatuses.BLACK)
        desk._board[9][11].set_status(CellStatuses.BLACK)
        desk._board[9][9].set_status(CellStatuses.BLACK)
        desk.update(CellStatuses.BLACK)
        self.assertTrue(desk._board[9][10].check_status(CellStatuses.EMPTY))

    '''
    bBEWE
    BEEWE
    bBEWE
    BbBWE
    bBEWB
    '''
    def test_update_for_capture(self):
        desk = Board(5)
        desk._board[0][1].set_status(CellStatuses.BLACK)
        desk._board[1][0].set_status(CellStatuses.BLACK)
        desk._board[0][3].set_status(CellStatuses.WHITE)
        desk._board[1][3].set_status(CellStatuses.WHITE)
        desk._board[2][3].set_status(CellStatuses.WHITE)
        desk._board[3][3].set_status(CellStatuses.WHITE)
        desk._board[4][3].set_status(CellStatuses.WHITE)
        desk._board[4][4].set_status(CellStatuses.BLACK)
        desk._board[2][1].set_status(CellStatuses.BLACK)
        desk._board[3][0].set_status(CellStatuses.BLACK)
        desk._board[3][2].set_status(CellStatuses.BLACK)
        desk._board[4][1].set_status(CellStatuses.BLACK)

        desk.update(CellStatuses.WHITE)

        self.assertTrue(desk._board[0][0].check_status(CellStatuses.BLACK_CAPTURED))
        self.assertFalse(desk._board[0][4].check_status(CellStatuses.BLACK_CAPTURED))
        self.assertFalse(desk._board[0][4].check_status(CellStatuses.WHITE_CAPTURED))

