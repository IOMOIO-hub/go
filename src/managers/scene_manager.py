from PyQt5.QtWidgets import QMainWindow

from core.utils.observer.iobserver import IObserver
from scenes.score_scene import ScoreScene

from scenes.types.iscene import IScene
from scenes.game_scene import GameScene
from scenes.menu_scene import MenuScene


class SceneManager(IObserver):

    def __init__(self, main_window: QMainWindow):
        self._main_window = main_window

        self._scene = None
        self.change_scene(MenuScene())

        self._main_window.setCentralWidget(self._scene)

    @property
    def scene(self) -> IScene:
        return self._scene

    def change_scene(self, next_scene: IScene) -> None:
        self._scene = next_scene
        self._scene.add_observer(self)
        self._main_window.setCentralWidget(self._scene)
        self._scene.installEventFilter(self._main_window)

    def notice(self, *args, **kwargs) -> None:
        {
            MenuScene.TO_GAME_SCENE: lambda: self.change_scene(GameScene(kwargs.get('desk_size'))),
            GameScene.TO_MENU_SCENE: lambda: self.change_scene(MenuScene()),
            ScoreScene.TO_MENU_SCENE: lambda: self.change_scene(MenuScene()),
            GameScene.TO_GAME_SCORE_SCENE: lambda: self.change_scene(ScoreScene(kwargs.get('score')))
        }[kwargs.get('event')]()
