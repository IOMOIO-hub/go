from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QPainter, QColor, QPen

from core.utils.assets import Assets
from objects.types.object import Object

from game_logic.cell import Cell
from game_logic.cell_statuses import CellStatuses


class Stone(Object, Cell):

    def __init__(self):
        Cell.__init__(self)

    def draw(self, painter: QPainter, size: int):
        match self.get_color():
            case CellStatuses.WHITE:
                painter.drawImage(QRect(0, 0, size, size), Assets.WHITE_STONE)
            case CellStatuses.BLACK:
                painter.drawImage(QRect(0, 0, size, size), Assets.BLACK_STONE)

        match self.get_captured():
            case CellStatuses.WHITE_CAPTURED:
                painter.fillRect(size // 4, size // 4, size // 2, size // 2, QColor(255, 255, 255))
            case CellStatuses.BLACK_CAPTURED:
                painter.fillRect(size // 4, size // 4, size // 2, size // 2, QColor(0, 0, 0))
