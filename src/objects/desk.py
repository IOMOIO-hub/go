from PyQt5.QtGui import QPainter, QColor, QPen
from yaml import safe_load

from core.utils.assets import Assets
from game_logic.cell_statuses import CellStatuses
from objects.types.object import Object

from game_logic.board import Board


class Desk(Object, Board):

    def __init__(self, size: int):

        Board.__init__(self, size)
        self.size = size
        self.padding = 75

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

    def draw(self, painter: QPainter):
        painter.setPen(QPen(QColor(24, 35, 46, 150), 3))

        cell_size = (self._config["window_width"] - self.padding * 2) // (self.size - 1)

        for i in range(self.size):
            painter.drawLine(self.padding + i * cell_size, self.padding,
                             self.padding + i * cell_size, self.padding + cell_size * (self.size - 1))

        for i in range(self.size):
            painter.drawLine(self.padding, self.padding + i * cell_size,
                             self.padding + cell_size * (self.size - 1), self.padding + i * cell_size)

        painter.translate(self.padding, self.padding)
        for i in range(self.size):
            for j in range(self.size):
                painter.translate(cell_size * j - cell_size // 3, cell_size * i - cell_size // 3)
                self._board[i][j].draw(painter, cell_size * 3 // 4)
                painter.translate(-cell_size * j + cell_size // 3, -cell_size * i + cell_size // 3)
        painter.translate(-self.padding, -self.padding)

    def add_stone(self, x, y, player) -> bool:
        cell_size = (self._config["window_width"] - self.padding * 2) // (self.size - 1)

        j, i = (x - self.padding + cell_size // 2) // cell_size, (y - self.padding + cell_size // 2) // cell_size
        if j < 0 or i < 0 or j >= self.size or i >= self.size:
            return False

        if self._board[i][j].check_status(CellStatuses.EMPTY):
            self._board[i][j].set_status(player)
            self.update(player)
            if (j, i) in self.if_dead({(j, i)}, player, (j, i)):
                self._board[i][j].set_status(CellStatuses.EMPTY)
                return False
            return True
        return False
