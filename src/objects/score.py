from PyQt5.QtGui import QPainter, QColor, QPen, QFont
from yaml import safe_load

from core.utils.assets import Assets
from objects.types.object import Object

class Score(Object):

    def __init__(self):
        self.padding = 75

        self.white_score = 0
        self.black_score = 0

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

    def update(self, white_score: int, black_score: int):
        self.white_score = white_score
        self.black_score = black_score

    def draw(self, painter: QPainter):
        painter.setPen(QPen(QColor(100, 100, 100), 5))
        painter.drawRect(40, self._config['window_width'], self._config['window_width'] - 80, 50)

        score = self.white_score + self.black_score
        if score == 0:
            return

        white_part = self.white_score / score
        black_part = self.black_score / score

        painter.fillRect(45, self._config['window_width'] + 5,
                         int((self._config['window_width'] - 90) * white_part), 40, QColor(255, 255, 255))

        painter.fillRect(self._config['window_width'] - 45 - int((self._config['window_width'] - 90) * black_part), self._config['window_width'] + 5,
                         int((self._config['window_width'] - 90) * black_part), 40, QColor(0, 0, 0))

        painter.setFont(QFont('inter', 16))
        painter.drawText(50, self._config['window_width'] + 35, str(self.white_score))
        painter.drawText(self._config['window_width'] - 85, self._config['window_width'] + 35, str(self.black_score))
