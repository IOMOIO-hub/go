import copy
from typing import Optional

from game_logic.cell import Cell
from game_logic.cell_statuses import CellStatuses
from objects.stone import Stone


class Board:
    def __init__(self, size: int):
        self._board = [[Stone() for _ in range(size)] for _ in range(size)]
        self._size = size
        self._points = {CellStatuses.WHITE_CAPTURED: 0, CellStatuses.BLACK_CAPTURED: 0}

    @property
    def points(self):
        return self._points

    def check_bounds(self, x: int, y: int) -> bool:
        return 0 <= x < self._size and 0 <= y < self._size

    @staticmethod
    def define_enemy(player: CellStatuses):
        return CellStatuses.WHITE if player == CellStatuses.BLACK else CellStatuses.BLACK

    def update_liberty(self) -> None:
        # update the liberties of all the available piece
        for y in range(self._size):
            for x in range(self._size):
                cell = self._board[y][x]
                count = 0
                directions = [(0, -1), (0, 1), (-1, 0), (1, 0)]
                if color := cell.get_color():
                    for direction in directions:
                        new_x, new_y = x + direction[0], y + direction[1]
                        if self.check_bounds(new_x, new_y):
                            neighbour_cell = self._board[new_x][new_y]
                            if neighbour_cell.get_color() == color or neighbour_cell.check_status(CellStatuses.EMPTY):
                                count += 1

                    cell.liberties = count

    def if_dead(self, dead_set: set[tuple[int, int]], chessman: CellStatuses, pos: tuple[int, int]) -> Optional[set[tuple[int, int]]]:
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]
        for i, j in directions:
            new_x = pos[0] + i
            new_y = pos[1] + j
            if (self.check_bounds(new_x, new_y) and (new_x, new_y) not in dead_set
                    and (self._board[new_y][new_x].check_status(CellStatuses.EMPTY))):
                return set()

        for i, j in directions:
            new_x = pos[0] + i
            new_y = pos[1] + j
            if (self.check_bounds(new_x, new_y) and ((new_x, new_y) not in dead_set)
                    and (self._board[new_y][new_x].check_status(chessman))):
                midvar = self.if_dead(dead_set | {(new_x, new_y)}, chessman,
                                      (new_x, new_y))
                if not midvar:
                    return set()
                else:
                    dead_set |= copy.deepcopy(midvar)

        return dead_set

    def get_dead_set(self, x: int, y: int, player: CellStatuses):
        dead_set = set()
        directions = [(-1, 0), (1, 0), (0, -1), (0, 1)]

        for i, j in directions:
            new_x = x + i
            new_y = y + j
            if (self.check_bounds(new_x, new_y) and
                    (self._board[new_y][new_x].check_status(self.define_enemy(player)) and ((new_x, new_y) not in dead_set))):
                kill_set = self.if_dead({(new_x, new_y)}, self.define_enemy(player), (new_x, new_y))
                if kill_set:
                    dead_set |= copy.deepcopy(kill_set)
        return dead_set

    def kill(self, kill_set: set[tuple[int, int]]) -> None:
        if len(kill_set) > 0:
            for dead_pos in kill_set:
                self._board[dead_pos[1]][dead_pos[0]].set_status(CellStatuses.EMPTY)

    def update(self, player: CellStatuses) -> None:
        for y in range(self._size):
            for x in range(self._size):
                self.kill(self.get_dead_set(x, y, player))
        self.mark_territories(self.find_territories())
        self.count_points()

    def find_territories(self):
        def dfs(x, y, visited, territory):
            if (x, y) in visited or not self.check_bounds(x, y):
                return
            visited.add((x, y))

            if not self._board[x][y].check_status(CellStatuses.EMPTY):
                return

            territory.add((x, y))
            dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]
            for dx, dy in dirs:
                new_x, new_y = x + dx, y + dy
                dfs(new_x, new_y, visited, territory)

        territories = []
        visited = set()

        for i in range(self._size):
            for j in range(self._size):
                if self._board[i][j].check_status(CellStatuses.EMPTY) and (i, j) not in visited:
                    new_territory = set()
                    dfs(i, j, visited, new_territory)
                    territories.append(new_territory)

        return territories

    def mark_territories(self, territories):
        for territory in territories:
            adjacent_colors = set()
            dirs = [(1, 0), (-1, 0), (0, 1), (0, -1)]
            for y, x in territory:
                for dy, dx in dirs:
                    new_y, new_x = y + dy, x + dx
                    if self.check_bounds(new_y, new_x):
                        adjacent_colors.add(self._board[new_y][new_x].get_color())
                if CellStatuses.WHITE in adjacent_colors and CellStatuses.BLACK in adjacent_colors:
                    break
            if CellStatuses.WHITE in adjacent_colors and CellStatuses.BLACK in adjacent_colors:
                self.clear_territory(territory)
            if CellStatuses.WHITE in adjacent_colors and CellStatuses.BLACK not in adjacent_colors:
                self.mark_territory(territory, CellStatuses.WHITE_CAPTURED)
            elif CellStatuses.WHITE not in adjacent_colors and CellStatuses.BLACK in adjacent_colors:
                self.mark_territory(territory, CellStatuses.BLACK_CAPTURED)

    def clear_territory(self, territory):
        for y, x in territory:
            self._board[y][x].delete_status(CellStatuses.WHITE_CAPTURED)
            self._board[y][x].delete_status(CellStatuses.BLACK_CAPTURED)

    def mark_territory(self, territory, color):
        for y, x in territory:
            self._board[y][x].set_status(color)

    def count_points(self):
        self._points[CellStatuses.WHITE_CAPTURED] = 0
        self._points[CellStatuses.BLACK_CAPTURED] = 0
        for row in self._board:
            for cell in row:
                if captured := cell.get_captured():
                    self._points[captured] += 1
                elif color := cell.get_color():
                    self._points[CellStatuses.WHITE_CAPTURED if color == CellStatuses.WHITE else CellStatuses.BLACK_CAPTURED] += 1

    def show(self):
        for i in range(self._size):
            for j in range(self._size):
                match self._board[i][j]:
                    case cell if cell.check_status(CellStatuses.WHITE_CAPTURED):
                        print('w', end='')
                    case cell if cell.check_status(CellStatuses.BLACK_CAPTURED):
                        print('b', end='')
                    case cell if cell.check_status(CellStatuses.EMPTY):
                        print('E', end='')
                    case cell if cell.check_status(CellStatuses.WHITE):
                        print('W', end='')
                    case cell if cell.check_status(CellStatuses.BLACK):
                        print('B', end='')


            print()
        print()
