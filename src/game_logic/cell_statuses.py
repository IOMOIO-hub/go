from enum import Enum


class CellStatuses(str, Enum):
    EMPTY = 'EMPTY'
    WHITE = 'WHITE'
    BLACK = 'BLACK'
    WHITE_CAPTURED = 'WHITE_CAPTURED'
    BLACK_CAPTURED = 'BLACK_CAPTURED'
