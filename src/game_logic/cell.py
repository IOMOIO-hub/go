from typing import Optional

from game_logic.cell_statuses import CellStatuses


class Cell:
    def __init__(self):
        self._statuses: dict[CellStatuses: bool] = {CellStatuses.EMPTY: True, CellStatuses.WHITE: False,
                                                    CellStatuses.BLACK: False, CellStatuses.BLACK_CAPTURED: False,
                                                    CellStatuses.WHITE_CAPTURED: False}
        self.liberties = 4

    def set_status(self, status: CellStatuses) -> None:
        self._statuses[status] = True
        match status:
            case CellStatuses.EMPTY:
                self._statuses[CellStatuses.WHITE] = False
                self._statuses[CellStatuses.BLACK] = False
            case CellStatuses.WHITE:
                self._statuses[CellStatuses.EMPTY] = False
                self._statuses[CellStatuses.BLACK_CAPTURED] = False
                self._statuses[CellStatuses.WHITE_CAPTURED] = False
            case CellStatuses.BLACK:
                self._statuses[CellStatuses.EMPTY] = False
                self._statuses[CellStatuses.BLACK_CAPTURED] = False
                self._statuses[CellStatuses.WHITE_CAPTURED] = False
            case CellStatuses.BLACK_CAPTURED:
                self._statuses[CellStatuses.WHITE_CAPTURED] = False
            case CellStatuses.WHITE_CAPTURED:
                self._statuses[CellStatuses.BLACK_CAPTURED] = False

    def delete_status(self, status: CellStatuses) -> None:
        self._statuses[status] = False

    def get_statuses(self) -> dict[CellStatuses: bool]:
        return self._statuses

    def get_color(self) -> Optional[CellStatuses]:
        if self.check_status(CellStatuses.EMPTY):
            return None
        return CellStatuses.WHITE if self.check_status(CellStatuses.WHITE) else CellStatuses.BLACK

    def get_captured(self) -> Optional[CellStatuses]:
        if self.check_status(CellStatuses.WHITE_CAPTURED) or self.check_status(CellStatuses.BLACK_CAPTURED):
            return CellStatuses.WHITE_CAPTURED if self.check_status(CellStatuses.WHITE_CAPTURED) else CellStatuses.BLACK_CAPTURED
        return None

    def check_status(self, status: CellStatuses) -> bool:
        return self._statuses[status]
