import sys
from os import path

from PyQt5.QtGui import QImage


def resource_path(relative: str) -> str:
    if hasattr(sys, "_MEIPASS"):
        return path.join(sys._MEIPASS, relative)
    return path.join(relative)


class Assets:
    _BASE_PATH = "assets"

    CONFIG_PATH = resource_path("config.yml")

    LOGO_PATH = resource_path(path.join(_BASE_PATH, "logo.png"))
    FONT_PATH = resource_path(path.join(_BASE_PATH, "Chequeblack.ttf"))

    WHITE_STONE_PATH = resource_path(path.join(_BASE_PATH, "white_stone.png"))
    BLACK_STONE_PATH = resource_path(path.join(_BASE_PATH, "black_stone.png"))

    WHITE_STONE = QImage(WHITE_STONE_PATH)
    BLACK_STONE = QImage(BLACK_STONE_PATH)
