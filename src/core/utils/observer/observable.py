from core.utils.observer.iobserver import IObserver


class Observable:
    def __init__(self) -> None:
        self._observers: list[IObserver] = list()

    def notify_observers(self, *args, **kwargs) -> None:
        for observer in self._observers:
            observer.notice(*args, **kwargs)

    def add_observer(self, observer: IObserver) -> None:
        self._observers.append(observer)

    def remove_observer(self, observer: IObserver) -> None:
        self._observers.remove(observer)
