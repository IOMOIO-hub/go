from abc import ABC, abstractmethod


class IObserver(ABC):
    @abstractmethod
    def notice(self, *args, **kwargs) -> None:
        raise NotImplementedError
