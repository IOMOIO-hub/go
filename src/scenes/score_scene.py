from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from scenes.types.iscene import IScene

class ScoreScene(IScene, Observable):
    TO_MENU_SCENE = 'scene has been switched to the menu'

    def __init__(self, score):
        IScene.__init__(self)
        Observable.__init__(self)

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.score = score
        self.message = self.get_message()

        self.setup_layout()

    def setup_layout(self):
        vbox = QVBoxLayout()
        vbox.setAlignment(Qt.AlignCenter)
        vbox.setSpacing(20)

        font_id = QFontDatabase.addApplicationFont(Assets.FONT_PATH)
        font_family = QFontDatabase.applicationFontFamilies(font_id)[0]
        custom_font = QFont(font_family, 24)

        label = QLabel(self)
        label.setText(self.message)
        label.setFont(custom_font)
        label.setAlignment(Qt.AlignCenter)

        button_style = '''
                    background-color: #1c2943;
                    color: white;
                    padding: 10px;
                    margin: 0 100px;
                    font-size: 24px;
                    border-radius: 10px;
                '''

        button = QPushButton('Меню', self)
        button.setStyleSheet(button_style)
        button.setFont(custom_font)
        button.clicked.connect(
            lambda: self.notify_observers(event=self.TO_MENU_SCENE)
        )

        vbox.addWidget(label)
        vbox.addWidget(button)
        self.setLayout(vbox)

    def get_message(self):
        if self.score.white_score > self.score.black_score:
            return 'Белые победили'
        if self.score.white_score == self.score.black_score:
            return 'Ничья'
        return 'Черные победили'

    def paintEvent(self, event):
        self.score.draw(QPainter(self))

    def key_pressed(self, key):
        keyboard = {
            Qt.Key_M: lambda: self.notify_observers(event=self.TO_MENU_SCENE),
        }
        if key in keyboard:
            keyboard[key]()

    def mouse_clicked(self, x, y):
        pass
