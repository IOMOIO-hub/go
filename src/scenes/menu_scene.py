from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from scenes.types.iscene import IScene


class MenuScene(IScene, Observable):
    TO_GAME_SCENE = 'scene has been switched to the game'

    def __init__(self):
        IScene.__init__(self)
        Observable.__init__(self)

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.setup_layout()

    def setup_layout(self):
        vbox = QVBoxLayout()
        vbox.setAlignment(Qt.AlignCenter)
        vbox.setSpacing(20)

        button_style = '''
            background-color: #1c2943;
            color: white;
            padding: 10px;
            margin: 0 100px;
            font-size: 24px;
            border-radius: 10px;
        '''

        select_style = '''
            font-size: 24px;
        '''

        font_id = QFontDatabase.addApplicationFont(Assets.FONT_PATH)
        font_family = QFontDatabase.applicationFontFamilies(font_id)[0]
        custom_font = QFont(font_family, 24)

        logo = QLabel(self)
        pixmap = QPixmap(Assets.LOGO_PATH)
        logo.setPixmap(pixmap)
        logo.setAlignment(Qt.AlignCenter)
        logo.setFixedHeight(300)
        logo.setFixedWidth(300)

        select = QComboBox()
        select.setStyleSheet(select_style)
        select.setFont(custom_font)

        self.select_value = 9
        select.currentTextChanged.connect(self.select_value_changed)
        select.addItems(['9x9', '13x13', '19x19'])

        play_button = QPushButton('Играть', self)
        play_button.setStyleSheet(button_style)
        play_button.setFont(custom_font)
        play_button.clicked.connect(
            lambda: self.notify_observers(event=self.TO_GAME_SCENE, desk_size=self.select_value)
        )

        vbox.addWidget(logo)
        vbox.addWidget(select)
        vbox.addWidget(play_button)

        self.setLayout(vbox)

    def select_value_changed(self, value):
        self.select_value = int(value.split('x')[0])