from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import QPainter

from core.utils.assets import Assets
from core.utils.observer.observable import Observable
from game import Game
from scenes.types.iscene import IScene


class GameScene(IScene, Observable):
    TO_MENU_SCENE = 'scene has been switched to the menu'
    TO_GAME_SCENE = 'scene has been switched to the game'
    TO_GAME_SCORE_SCENE = 'scene has been switched to the score'

    def __init__(self, board_size: int):
        super().__init__()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self._game = Game(board_size)
        self._game.add_observer(self)

    def update_game(self):
        self._game.update()
        self.update()

    def paintEvent(self, event):
        self._game.draw(QPainter(self))

    def key_pressed(self, key):
        keyboard = {
            Qt.Key_M: lambda: self.notify_observers(event=self.TO_MENU_SCENE),
            Qt.Key_P: lambda: self._game.skip_turn()
        }
        if key in keyboard:
            keyboard[key]()

    def mouse_clicked(self, x, y):
        self._game.place_stone(x, y)
        self.update_game()

    def notice(self, *args, **kwargs):
        if kwargs.get('event') == 'GAME OVER':
            self.notify_observers(event=self.TO_GAME_SCORE_SCENE, score=kwargs.get('score'))