from yaml import safe_load

from PyQt5.QtCore import *
from PyQt5.QtGui import *

from core.utils.assets import Assets
from core.utils.observer.observable import Observable

from objects.desk import Desk
from game_logic.cell_statuses import CellStatuses
from objects.score import Score


class Game(Observable):

    def __init__(self, desk_size: int):
        Observable.__init__(self)

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.desk_size = desk_size
        self.desk = Desk(desk_size)
        self.score = Score()
        self.player = CellStatuses.WHITE

        self.is_prev_turn_skipped = False

    def update(self):
        self.desk.update(self.player)
        self.score.update(self.desk.points[CellStatuses.WHITE_CAPTURED], self.desk.points[CellStatuses.BLACK_CAPTURED])

    def draw(self, painter: QPainter):
        painter.fillRect(0, 0, self._config['window_width'], self._config['window_height'], QColor(201, 205, 214))
        painter.fillRect(40, 40, self._config['window_width'] - 80, self._config['window_width'] - 80, QColor(220, 165, 97))
        painter.setPen(QPen(QColor(226, 183, 128), 10))
        painter.drawRoundedRect(35, 35, self._config['window_width'] - 70, self._config['window_width'] - 70, 25, 25)

        self.desk.draw(painter)
        self.score.draw(painter)

    def switch_player(self):
        match self.player:
            case CellStatuses.WHITE:
                self.player = CellStatuses.BLACK
            case CellStatuses.BLACK:
                self.player = CellStatuses.WHITE

    def place_stone(self, x, y):
        if self.desk.add_stone(x, y, self.player):
            self.switch_player()
            self.is_prev_turn_skipped = False

    def skip_turn(self):
        if self.is_prev_turn_skipped:
            self.notify_observers(event='GAME OVER', score=self.score)
        else:
            self.switch_player()
            self.is_prev_turn_skipped = True
