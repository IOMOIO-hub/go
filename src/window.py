from PyQt5.QtCore import QEvent, Qt
from yaml import safe_load

from PyQt5.QtWidgets import QMainWindow

from core.utils.assets import Assets
from managers.scene_manager import SceneManager


class Window(QMainWindow):

    def __init__(self):
        super().__init__()

        with open(Assets.CONFIG_PATH, 'r') as f:
            self._config = safe_load(f)

        self.resize(self._config['window_width'], self._config['window_height'])
        self.setWindowTitle("Go")

        self.scene_manager = SceneManager(self)
        self.scene_manager._scene.installEventFilter(self)

    def keyPressEvent(self, event):
        self.scene_manager._scene.key_pressed(event.key())

    def keyReleaseEvent(self, event):
        self.scene_manager._scene.key_released(event.key())

    def eventFilter(self, obj, event):
        if event.type() == QEvent.MouseMove:
            x = event.x()
            y = event.y()

        elif event.type() == QEvent.MouseButtonPress and event.button() == Qt.LeftButton:
            self.scene_manager._scene.mouse_clicked(event.x(), event.y())

        return super().eventFilter(obj, event)
